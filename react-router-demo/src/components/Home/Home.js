
import React from 'react';
import './style.css';

const Home = () => {
  return (
    <div className="home-container">
      <h1>Hi, there !</h1>
      <h2>My name is Srinivas</h2>
      <h3>Software Engineer</h3>
    </div>
  )
}

export default Home;