import React from 'react';
import './style.css';

const About = () => {
  return (
    <div className='about-bg-container'>
      <div className='info-container'>
        <h3 className='about'>About Me</h3>
        <p>I'm from Warangal, Telangana state</p>
        <p>My parents are formers</p>
        <p>I have 2 siblings</p>
        <p>I love spending time with family</p>
      </div>
      <div className='img-container'>
        <img src="https://res.cloudinary.com/db5furjnj/image/upload/v1651469074/2020-10-24-10-13-01-127_wlbe8o.jpg" alt='profile' className='profile-img' />
      </div>
    </div>
  )
}

export default About;