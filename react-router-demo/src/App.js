import './App.css';
import { Routes, Route } from 'react-router-dom';
import  About  from './components/About/About';
import  Home  from './components/Home/Home';
import  Navbar  from './components/Navbar/Navbar';
import ContactUs from './components//ContactUs/ContactUs';

function App() {
  return (
    <>
    <Navbar />
    <Routes>
      <Route path='/' element={<Home />} />
      <Route path='about' element={<About />} />
      <Route path='contact' element={<ContactUs />} />
    </Routes>
    </>
  );
}

export default App;
