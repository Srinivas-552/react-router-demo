import React, { Component } from 'react';
import './style.css';

class FormData extends Component {
    constructor(props){
        super(props)
  
        this.state = {
          username: '',
          contactNum: '',
          cautionMessage: {}
        }
      }
  
      handleUsernameChange = (event) => {
        this.setState({
          cautionMessage: {success: ""},
          username: event.target.value
        })
      }
  
      handleContactNumChange = (event) => {
        this.setState({
          cautionMessage: {success: ""},
          contactNum: event.target.value
        })
      }

      handleSubmit = (event) => {
          event.preventDefault()
        
          const {username, contactNum, cautionMessage} = this.state

          if(username===""){
            this.setState(prevState => ({
              cautionMessage: {...prevState.cautionMessage, name: 'Username is Required*'}
            }))
          }else{
            this.setState(prevState => ({
              cautionMessage: {...prevState.cautionMessage, name: ''}
            }))
          }

          if(contactNum==="" || contactNum.length !== 10){
            this.setState(prevState => ({
              cautionMessage: {...prevState.cautionMessage, number: 'Contact number is Required*'}
            }))
          }else{
            this.setState(prevState => ({
              cautionMessage: {...prevState.cautionMessage, name: ''}
            }))
          }

          if(username !== "" && contactNum !== "" && contactNum.length === 10){
            this.setState(prevState => ({
              cautionMessage: {...prevState.cautionMessage, success: 'Successfully Submitted'},
              username: "",
              contactNum: ""
            }))          
          }
        
      }

  render() {
    const {username, contactNum, cautionMessage} = this.state
    
    return (
        <div className='form-container'>  
            <form onSubmit={this.handleSubmit}>
              
              <label className='label'>Username</label>
              <input
              type="text" name='username' id='username' value={username} onChange={this.handleUsernameChange} placeholder='Enter your name' />
              <p className='error'>{cautionMessage.name}</p>


              <label htmlFor='telephone' className='label'>Telephone</label>
              <input type="tel" name='contactNum' value={contactNum} onChange={this.handleContactNumChange} placeholder='Enter your number' />
              <p className='error'>{cautionMessage.number}</p>

              <button type='submit' className='button'>Submit</button>
              <p className='success'>{cautionMessage.success}</p>
            </form>
        </div> 
    )
  }
}

export default FormData